import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import Home from "./components/utilities/home";
import Header from "./components/utilities/header";
import BoardView from "./components/board/boardView";
import CardView from "./components/card/cardView";

function App() {
  return (
    <React.Fragment>
      <Header />
      <Router>
        <Route exact path="/" component={Home}></Route>
        <Route exact path="/home" component={Home}></Route>
        <Route exact path="/board/:id" component={BoardView}></Route>
        <Route exact path="/card/:id" component={CardView}></Route>
      </Router>
    </React.Fragment>
  );
}

export default App;
