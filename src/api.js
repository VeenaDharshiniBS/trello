import axios from "axios";

const BASE_URL = "https://api.trello.com/1/";
const API_KEY = "05784795cc3442095e501a7a665f37d5";
const TOKEN =
  "ATTA9f17ad9ad4ee71d94f14578e66b3b3d2271d79d0d1f15a35089d110c543f14fb71EAD63A";
const MEMBER = "veenadharshini";

class Api {
  //Boards
  getAllBoards() {
    return axios.get(
      `${BASE_URL}members/${MEMBER}/boards?key=${API_KEY}&token=${TOKEN}`
    );
  }

  createABoard(boardTitle, backgroundColor) {
    return axios.post(BASE_URL + "boards/", {
      name: boardTitle,
      prefs_background: backgroundColor == "" ? "blue" : backgroundColor,
      key: API_KEY,
      token: TOKEN,
    });
  }

  deleteABoard(boardId) {
    return axios.delete(
      `${BASE_URL}boards/${boardId}?&key=${API_KEY}&token=${TOKEN}`
    );
  }

  getABoard(boardId) {
    return axios.get(
      `${BASE_URL}boards/${boardId}?key=${API_KEY}&token=${TOKEN}`
    );
  }

  //Lists
  getAllListsOfABoard(boardId) {
    return axios.get(
      `${BASE_URL}boards/${boardId}/lists?key=${API_KEY}&token=${TOKEN}`
    );
  }

  createAList(boardId, listName) {
    return axios.post(
      `${BASE_URL}boards/${boardId}/lists?name=${listName}&key=${API_KEY}&token=${TOKEN}`
    );
  }

  archiveAList(listId, close) {
    return axios.put(
      `${BASE_URL}lists/${listId}/?closed=${close}&key=${API_KEY}&token=${TOKEN}`
    );
  }

  //Cards
  getAllCardsOfAList(listId) {
    return axios.get(
      `${BASE_URL}lists/${listId}/cards?key=${API_KEY}&token=${TOKEN}`
    );
  }

  createACard(listId, cardName) {
    return axios.post(
      `${BASE_URL}cards?idList=${listId}&name=${cardName}&key=${API_KEY}&token=${TOKEN}`
    );
  }

  deleteACard(cardId) {
    return axios.delete(
      `${BASE_URL}cards/${cardId}?key=${API_KEY}&token=${TOKEN}`
    );
  }

  getACard(cardId) {
    return axios.get(
      `${BASE_URL}cards/${cardId}?key=${API_KEY}&token=${TOKEN}`
    );
  }

  //Checklist
  getCheckListsOfACard(cardId) {
    return axios.get(
      `${BASE_URL}cards/${cardId}/checklists?key=${API_KEY}&token=${TOKEN}`
    );
  }

  createAChecklist(cardId, checklistName) {
    return axios.post(
      `${BASE_URL}checklists?idCard=${cardId}&key=${API_KEY}&token=${TOKEN}&name=${checklistName}`
    );
  }

  deleteAChecklist(cardId) {
    return axios.delete(
      `${BASE_URL}checklists/${cardId}?key=${API_KEY}&token=${TOKEN}`
    );
  }

  //CheckItem
  getCheckItems(checklistId) {
    return axios.get(
      `${BASE_URL}checklists/${checklistId}/checkItems?key=${API_KEY}&token=${TOKEN}`
    );
  }

  createCheckItem(checklistId, checkItemname) {
    return axios.post(
      `${BASE_URL}checklists/${checklistId}/checkItems?name=${checkItemname}&key=${API_KEY}&token=${TOKEN}`
    );
  }
}

export default new Api();
