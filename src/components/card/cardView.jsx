import React, { Component } from "react";
import { Modal } from "react-bootstrap";
import Checklist from "../checklist/checklist";
import NotFound from "../utilities/notFound";
import Loader from "../utilities/loader";
import Api from "../../api";
import CreateChecklist from "../checklist/createChecklist";
import ToastMsg from "../utilities/toastMsg";

class CardView extends Component {
  state = {
    checklists: [],
    loading: true,
    errorDisplay: false,
    showPopover: false,
    message: "",
  };

  handleNewChecklist = (newChecklist) => {
    this.setState({ checklists: [...this.state.checklists, newChecklist] });
  };

  handleDeleteChecklist = (checklistId) => {
    let newChecklist = this.state.checklists.filter(
      (checklist) => checklist.id !== checklistId
    );
    this.setState({
      checklists: newChecklist,
    });
  };

  handleMessage = (res) => {
    this.setState({ message: res });
  };

  componentDidMount() {
    const cardId = this.props.match.params.id;
    const fetchData = async () => {
      this.setState({ loading: true });
      try {
        const response = await Api.getCheckListsOfACard(cardId);
        this.setState({ checklists: response.data });
      } catch (error) {
        this.setState({ errorDisplay: true });
        console.log(error);
      }
      this.setState({ loading: false });
    };
    fetchData();
  }

  render() {
    return (
      <>
        {!this.state.errorDisplay && this.state.loading && <Loader></Loader>}
        {this.state.errorDisplay && <NotFound />}
        <Modal show="true">
          <CreateChecklist
            cardId={this.props.match.params.id}
            onAdd={this.handleNewChecklist}
            onGetMsg={this.handleMessage}
          />

          <Modal.Body>
            {this.state.showPopover && <CreateChecklist />}
            {!this.state.errorDisplay &&
              this.state.checklists &&
              this.state.checklists.length > 0 &&
              this.state.checklists.map((checklist) => (
                <div key={checklist.id}>
                  <Checklist
                    checklist={checklist}
                    onDeleteCheklist={this.handleDeleteChecklist}
                    onGetMsg={this.handleMessage}
                  />
                </div>
              ))}
          </Modal.Body>

          {this.state.message && (
            <div style={{ display: "flex", justifyContent: "center" }}>
              <ToastMsg message={this.state.message} />
            </div>
          )}
        </Modal>
      </>
    );
  }
}

export default CardView;
