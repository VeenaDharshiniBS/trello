import React, { Component } from "react";
import { ListGroup, CloseButton, Button, Form } from "react-bootstrap";
import Api from "../../api";

class CreateCard extends Component {
  state = {
    cardName: "",
    showForm: false,
  };

  handleSubmit = (event) => {
    event.preventDefault();

    const fetchData = async () => {
      try {
        const response = await Api.createACard(
          this.props.listId,
          this.state.cardName
        );
        this.props.onAddNewCard(response.data);
        this.props.onGetMsg(response.data.name + " card added");
        this.setState({ cardName: "" });
      } catch (error) {
        console.log(error);
      }
    };
    fetchData();
    this.handleClose();
  };

  handleOpen() {
    this.setState({ showForm: true });
  }

  handleClose() {
    this.setState({ showForm: false });
  }

  render() {
    return (
      <ListGroup.Item style={{ cursor: "pointer" }}>
        {!this.state.showForm && (
          <div onClick={() => this.handleOpen()}>+ Add a card</div>
        )}
        {this.state.showForm && (
          <Form onSubmit={this.handleSubmit}>
            <Form.Group controlId="cardName">
              <Form.Control
                type="text"
                value={this.state.cardName}
                onChange={(e) => this.setState({ cardName: e.target.value })}
                placeholder="Enter a title for this card"
              />
            </Form.Group>

            <Button
              disabled={this.state.cardName === ""}
              variant="secondary"
              type="submit"
            >
              Add card
            </Button>
            <CloseButton onClick={() => this.handleClose()} />
          </Form>
        )}
      </ListGroup.Item>
    );
  }
}

export default CreateCard;
