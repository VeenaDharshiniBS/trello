import React, { Component } from "react";
import { ListGroup, Dropdown } from "react-bootstrap";
import { Link } from "react-router-dom";
import Api from "../../api";

class CardComp extends Component {
  state = {};

  handleDeleteCard() {
    const fetchData = async () => {
      try {
        const response = await Api.deleteACard(this.props.card.id);
        if (response.status == 200) {
          this.props.onDeleteCard(this.props.card.id);
          this.props.onGetMsg("Card removed");
        }
      } catch (error) {
        console.log(error);
      }
    };
    fetchData();
  }

  render() {
    return (
      <ListGroup.Item style={{ display: "flex", flexDirection: "row" }}>
        <Link style={{ flex: "90%" }} to={`/card/${this.props.card.id}`}>
          <div>{this.props.card.name}</div>
        </Link>

        <Dropdown style={{ flex: "10%" }}>
          <Dropdown.Toggle
            id="dropdown-basic"
            style={{
              backgroundColor: "transparent",
              border: "none",
              color: "black",
            }}
          ></Dropdown.Toggle>

          <Dropdown.Menu>
            <Dropdown.Item>View Card</Dropdown.Item>
            <Dropdown.Item>Edit Card</Dropdown.Item>
            <Dropdown.Item onClick={() => this.handleDeleteCard()}>
              Delete Card
            </Dropdown.Item>
          </Dropdown.Menu>
        </Dropdown>
      </ListGroup.Item>
    );
  }
}

export default CardComp;
