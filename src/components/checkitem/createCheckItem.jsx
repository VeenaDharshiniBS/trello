import React, { Component } from "react";
import { Form, Button, CloseButton, Badge } from "react-bootstrap";
import Api from "../../api";

class CreateCheckItem extends Component {
  state = {
    checkItemName: "",
    showForm: false,
  };

  handleSubmit = (event) => {
    event.preventDefault();

    const fetchData = async () => {
      try {
        const response = await Api.createCheckItem(
          this.props.checklist.id,
          this.state.checkItemName
        );
        console.log(response.data);
        this.props.onAddNewCheckItem(response.data);
        this.setState({ checkItemName: "" });
      } catch (error) {
        console.log(error);
      }
    };
    fetchData();
    this.handleClose();
  };

  handleOpen() {
    this.setState({ showForm: true });
  }

  handleClose() {
    this.setState({ showForm: false });
  }

  render() {
    return (
      <>
        <Button onClick={() => this.handleOpen()} size="sm">
          Add an item
        </Button>
        {this.state.showForm && (
          <Form onSubmit={this.handleSubmit}>
            <Form.Group controlId="checkItemName">
              <Form.Control
                type="text"
                value={this.state.checkItemName}
                onChange={(e) =>
                  this.setState({ checkItemName: e.target.value })
                }
                placeholder="Add an item"
              />
            </Form.Group>

            <Button disabled={this.state.checkItemName === ""} type="submit">
              Add
            </Button>
            <CloseButton onClick={() => this.handleClose()} />
          </Form>
        )}
      </>
    );
  }
}

export default CreateCheckItem;
