import React, { Component } from "react";
import Form from "react-bootstrap/Form";

class CheckItem extends Component {
  state = {};

  render() {
    const { checkItem } = this.props;
    return (
      <div>
        <Form.Check
          inline
          label={checkItem.name}
          name="group1"
          type="checkbox"
          id={`inline-checkbox-1`}
          defaultChecked={checkItem.state != "incomplete"}
        />
      </div>
    );
  }
}

export default CheckItem;
