import React, { Component } from "react";
import { Col, Card } from "react-bootstrap";
import Board from "./board";
import CreateBoard from "./createBoard";
import Loader from "../utilities/loader";
import NotFound from "../utilities/notFound";
import Api from "../../api";

class Boards extends Component {
  state = {
    boards: [],
    visible: false,
    loading: true,
    errorDisplay: false,
  };

  constructor(props) {
    super(props);
    this.handleOpen = this.handleOpen.bind(this);
    this.handleClose = this.handleClose.bind(this);
  }

  handleOpen() {
    this.setState({ visible: true });
  }

  handleClose() {
    this.setState({ visible: false });
  }

  handleNewBoard = (newBoard) => {
    this.setState({ boards: [...this.state.boards, newBoard] });
  };

  handleDeleteBoard = (boardId) => {
    this.setState({
      boards: this.state.boards.filter((board) => board.id != boardId),
    });
  };

  componentDidMount() {
    const fetchData = async () => {
      this.setState({ loading: true });
      try {
        const response = await Api.getAllBoards();
        this.setState({ boards: response.data });
      } catch (error) {
        this.setState({ errorDisplay: true });
      }
      this.setState({ loading: false });
    };
    fetchData();
  }

  render() {
    return (
      <>
        {!this.state.errorDisplay && this.state.loading && <Loader></Loader>}
        {!this.state.errorDisplay && (
          <Col
            style={{
              width: "250px",
              height: "100px",
              cursor: "pointer",
              margin: "15px",
            }}
          >
            <Card
              style={{
                width: "250px",
                height: "100px",
                cursor: "pointer",
                margin: "15px",
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
              }}
              onClick={() => this.handleOpen()}
            >
              Create a board
            </Card>
          </Col>
        )}
        {this.state.errorDisplay && <NotFound />}
        {this.state.visible && (
          <CreateBoard
            onClose={this.handleClose}
            visible={this.state.visible}
            onAdd={this.handleNewBoard}
            onGetMsg={this.props.onGetMsg}
          />
        )}
        {!this.state.errorDisplay &&
          this.state.boards &&
          this.state.boards.length > 0 &&
          this.state.boards.map((board) => (
            <Col
              key={board.id}
              style={{
                width: "250px",
                height: "100px",
                cursor: "pointer",
                margin: "15px",
              }}
            >
              <Board
                board={board}
                onDeleteBoard={this.handleDeleteBoard}
                onGetMsg={this.props.onGetMsg}
              />
            </Col>
          ))}
      </>
    );
  }
}

export default Boards;
