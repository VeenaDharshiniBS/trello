import React, { Component } from "react";
import { Dropdown, Card } from "react-bootstrap";
import { Link } from "react-router-dom";
import Api from "../../api";

class Board extends Component {
  state = {};

  handleDeleteBoard() {
    const fetchData = async () => {
      try {
        const response = await Api.deleteABoard(this.props.board.id);
        if (response.status == 200) {
          this.props.onDeleteBoard(this.props.board.id);
          this.props.onGetMsg("Board deleted");
        }
      } catch (error) {
        console.log(error);
      }
    };
    fetchData();
  }

  render() {
    const { board } = this.props;
    //console.log(board);
    return (
      <Card
        className="text-white m-2"
        style={{
          width: "250px",
          height: "100px",
          backgroundColor: board.prefs.backgroundColor,
        }}
      >
        <Card.Img
          src={board.prefs.backgroundImage}
          alt=""
          style={{
            overflow: "hidden",
            opacity: 0.7,
          }}
        />
        <Card.ImgOverlay
          style={{
            backgroundColor: board.prefs.backgroundColor,
            display: "flex",
            flexDirection: "row",
          }}
        >
          <Link style={{ flex: 4 }} to={`/board/${this.props.board.id}`}>
            <Card.Title style={{ color: "white" }}>{board.name}</Card.Title>
          </Link>
          <Dropdown style={{ flex: 1 }}>
            <Dropdown.Toggle
              style={{
                backgroundColor: "transparent",
                border: "none",
                color: "black",
              }}
              id="dropdown-basic"
            ></Dropdown.Toggle>

            <Dropdown.Menu>
              <Dropdown.Item>View the board</Dropdown.Item>
              <Dropdown.Item onClick={() => this.handleDeleteBoard()}>
                Delete the board
              </Dropdown.Item>
            </Dropdown.Menu>
          </Dropdown>
        </Card.ImgOverlay>
      </Card>
    );
  }
}

export default Board;
