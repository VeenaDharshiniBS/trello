import React, { Component } from "react";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import Form from "react-bootstrap/Form";
import Api from "../../api";
import BgColorPicker from "../utilities/bgColorPicker";

class CreateBoard extends Component {
  state = {
    boardTitle: "",
    backgroundColor: "",
    visibility: "workspace",
  };

  handleSubmit = (event) => {
    event.preventDefault();

    const fetchData = async () => {
      try {
        const response = await Api.createABoard(
          this.state.boardTitle,
          this.state.backgroundColor
        );
        if (response.status == 200) {
          this.props.onGetMsg(response.data.name + " board added");
          this.props.onAdd(response.data);
        }
      } catch (error) {
        console.log(error);
      }
    };
    fetchData();

    this.props.onClose();
  };

  handleBGColor = (color) => {
    this.setState({ backgroundColor: color });
  };

  render() {
    return (
      <Modal
        show={this.props.visible}
        size="sm"
        aria-labelledby="contained-modal-title-vcenter"
        centered
        onSubmit={this.handleSubmit}
      >
        <Modal.Header closeButton onClick={this.props.onClose}>
          <Modal.Title id="contained-modal-title-vcenter">
            Create a board
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div
            style={{
              backgroundColor:
                this.state.backgroundColor == "sky"
                  ? this.state.backgroundColor + "blue"
                  : this.state.backgroundColor,
            }}
          >
            <img
              src="https://a.trellocdn.com/prgb/assets/images/board-preview-skeleton.14cda5dc635d1f13bc48.svg"
              style={{ width: 250, margin: "auto" }}
            ></img>
          </div>

          <Form>
            <Form.Group className="mb-3" controlId="boardTitle">
              <Form.Label>Board Title</Form.Label>
              <Form.Control
                type="text"
                value={this.state.boardTitle}
                onChange={(e) => this.setState({ boardTitle: e.target.value })}
              />
              <Form.Text className="text-muted">
                Board title is required
              </Form.Text>
            </Form.Group>

            <BgColorPicker getColor={this.handleBGColor}></BgColorPicker>

            <Form.Select
              onChange={(e) => this.setState({ visibility: e.target.value })}
              aria-label="Default select example"
            >
              <option value="workspace">Workspace</option>
              <option value="private">Private</option>
              <option value="public">Public</option>
            </Form.Select>

            <Button variant="secondary" type="submit">
              Create
            </Button>
          </Form>
        </Modal.Body>
      </Modal>
    );
  }
}

export default CreateBoard;
