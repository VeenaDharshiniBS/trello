import React, { Component } from "react";
import Api from "../../api";
import Loader from "../utilities/loader";
import List from "../list/list";
import NotFound from "../utilities/notFound";
import CreateList from "../list/createList";
import ToastMsg from "../utilities/toastMsg";

class BoardView extends Component {
  state = {
    lists: [],
    loading: true,
    errorDisplay: false,
    toast: false,
    message: "",
  };

  handleNewList = (newList) => {
    this.setState({ lists: [...this.state.lists, newList] });
  };

  handleArchiveList = (listId) => {
    let newLists = this.state.lists.filter((list) => list.id != listId);
    this.setState({
      lists: newLists,
    });
  };

  handleMessage = (res) => {
    this.setState({ message: res });
  };

  componentDidMount() {
    const boardId = this.props.match.params.id;
    const fetchData = async () => {
      this.setState({ loading: true });
      try {
        const response = await Api.getAllListsOfABoard(boardId);
        this.setState({ lists: response.data });
        const bgColor = await Api.getABoard(boardId);
        document.querySelector("body").style.backgroundColor =
          bgColor.data.prefs.backgroundColor;
      } catch (error) {
        this.setState({ errorDisplay: true });
      }
      this.setState({ loading: false });
    };
    fetchData();
  }

  render() {
    const mystyle = {
      display: "grid",
      gridAutoFlow: "column",
      gap: "20px",
    };
    return (
      <>
        {!this.state.errorDisplay && this.state.loading && <Loader></Loader>}
        {this.state.errorDisplay && <NotFound />}
        {!this.state.errorDisplay && (
          <div style={mystyle}>
            <div style={{ cursor: "pointer" }}>
              <CreateList
                boardId={this.props.match.params.id}
                onAddNewList={this.handleNewList}
                style={{ boxSizing: "border-box", width: "400px" }}
                onGetMsg={this.handleMessage}
              ></CreateList>
            </div>
            {this.state.lists &&
              this.state.lists.length > 0 &&
              this.state.lists.map((list) => (
                <div key={list.id}>
                  <List
                    onArchiveList={this.handleArchiveList}
                    list={list}
                    onGetMsg={this.handleMessage}
                  />
                </div>
              ))}
          </div>
        )}
        {this.state.message && (
          <div style={{ display: "flex", justifyContent: "center" }}>
            <ToastMsg message={this.state.message} />
          </div>
        )}
      </>
    );
  }
}

export default BoardView;
