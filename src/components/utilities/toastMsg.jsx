import React, { Component } from "react";
import Toast from "react-bootstrap/Toast";

class ToastMsg extends Component {
  state = {
    show: true,
  };

  render() {
    return (
      <Toast
        onClose={() => this.setState({ show: false })}
        show={this.state.show}
        autohide
      >
        <Toast.Header>
          <strong className="me-auto">Done</strong>
          <small>Just Now</small>
        </Toast.Header>
        <Toast.Body>{this.props.message}</Toast.Body>
      </Toast>
    );
  }
}

export default ToastMsg;
