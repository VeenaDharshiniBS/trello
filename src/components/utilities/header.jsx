import React from "react";
import Container from "react-bootstrap/Container";
import Navbar from "react-bootstrap/Navbar";

function Header() {
  return (
    <Navbar bg="primary" variant="dark">
      <Container>
        <a href="/home">
          <Navbar.Brand>
            <img
              src={require("../../logo.png")}
              width="30px"
              height="30px"
              className="d-inline-block align-top mx-3"
            />
            Trello
          </Navbar.Brand>
        </a>
      </Container>
    </Navbar>
  );
}

export default Header;
