import React, { Component } from "react";
import { Container, Row } from "react-bootstrap";
import Boards from "../board/boards";
import ToastMsg from "./toastMsg";

class Home extends Component {
  state = {
    message: "",
  };

  handleMessage = (res) => {
    this.setState({ message: res });
  };

  render() {
    document.querySelector("body").style.backgroundColor = "#f4f4f4";
    return (
      <>
        <Container>
          <Row>
            <Boards onGetMsg={this.handleMessage}></Boards>
          </Row>
        </Container>
        {this.state.message && (
          <div style={{ display: "flex", justifyContent: "center" }}>
            <ToastMsg message={this.state.message} />
          </div>
        )}
      </>
    );
  }
}

export default Home;
