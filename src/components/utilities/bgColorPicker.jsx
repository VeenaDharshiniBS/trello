import React, { Component } from "react";
import { Container, Row, Col } from "react-bootstrap";

class BgColorPicker extends Component {
  render() {
    const colors = [
      "blue",
      "orange",
      "green",
      "red",
      "purple",
      "pink",
      "lime",
      "sky",
      "grey",
    ];
    return (
      <Container style={{ cursor: "pointer" }}>
        <Row>
          {colors &&
            colors.map((color, index) => (
              <Col
                style={{
                  backgroundColor: color == "sky" ? color + "blue" : color,
                }}
                onClick={() => this.props.getColor(color)}
                key={index + 1}
              >
                <div style={{ width: "50px", height: "30px" }}></div>
              </Col>
            ))}
        </Row>
      </Container>
    );
  }
}

export default BgColorPicker;
