import React, { Component } from "react";
import { Button, Form, CloseButton } from "react-bootstrap";
import Api from "../../api";

class CreateChecklist extends Component {
  state = {
    checklistName: "Checklist",
    showForm: false,
  };

  handleSubmit = (event) => {
    event.preventDefault();

    const fetchData = async () => {
      try {
        const response = await Api.createAChecklist(
          this.props.cardId,
          this.state.checklistName
        );
        this.props.onGetMsg("Checklist added");
        this.props.onAdd(response.data);
      } catch (error) {
        console.log(error);
      }
    };
    fetchData();
  };

  render() {
    return (
      <>
        <Button
          onClick={() => this.setState({ showForm: true })}
          variant="secondary"
        >
          Add a checklist
        </Button>
        <div>
          {this.state.showForm && (
            <Form onSubmit={this.handleSubmit}>
              <Form.Group className="mb-3" controlId="checklistTitle">
                <Form.Label>Title</Form.Label>
                <Form.Control
                  type="text"
                  value={this.state.checklistName}
                  onChange={(e) =>
                    this.setState({ checklistName: e.target.value })
                  }
                />
              </Form.Group>

              <Button variant="primary" type="submit" size="sm">
                Add
              </Button>
              <CloseButton
                onClick={() => this.setState({ showForm: false })}
              ></CloseButton>
            </Form>
          )}
        </div>
      </>
    );
  }
}

export default CreateChecklist;
