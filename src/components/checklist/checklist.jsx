import React, { Component } from "react";
import { Button, Card, ProgressBar } from "react-bootstrap";
import NotFound from "../utilities/notFound";
import Loader from "../utilities/loader";
import Api from "../../api";
import CreateCheckItem from "../checkitem/createCheckItem";
import CheckItem from "../checkitem/checkItem";

class Checklist extends Component {
  state = {
    checkItems: [],
    loading: true,
    errorDisplay: false,
    progressPercent: 0,
  };

  handleDeleteChecklist() {
    const fetchData = async () => {
      try {
        const response = await Api.deleteAChecklist(this.props.checklist.id);
        if (response.status == 200) {
          this.props.onDeleteCheklist(this.props.checklist.id);
          this.props.onGetMsg("Checklist removed");
        }
      } catch (error) {
        console.log(error);
      }
    };
    fetchData();
  }

  handleNewCheckItem = (newCheckItem) => {
    this.setState({ checkItems: [...this.state.checkItems, newCheckItem] });
  };

  componentDidMount() {
    const fetchData = async () => {
      this.setState({ loading: true });
      try {
        const response = await Api.getCheckItems(this.props.checklist.id);
        this.setState({ checkItems: response.data });
        console.log(response.data);
        this.setState({
          progressPercent:
            (response.data.filter((item) => item.state == "complete").length /
              response.data.length) *
            100,
        });
      } catch (error) {
        this.setState({ errorDisplay: true });
      }
      this.setState({ loading: false });
    };
    fetchData();
  }

  render() {
    const { checklist } = this.props;

    return (
      <>
        {!this.state.errorDisplay && this.state.loading && <Loader></Loader>}
        {this.state.errorDisplay && <NotFound />}
        {!this.state.errorDisplay && (
          <Card style={{ padding: "20px", margin: "10px" }}>
            <Card.Title
              style={{ display: "flex", justifyContent: "space-between" }}
            >
              <div style={{ flex: "1fr" }}>{checklist.name}</div>

              <Button
                variant="secondary"
                size="sm"
                onClick={() => this.handleDeleteChecklist()}
              >
                Delete
              </Button>
            </Card.Title>

            <Card.Body>
              <ProgressBar animated now={this.state.progressPercent} />
              {this.state.checkItems &&
                this.state.checkItems.map((checkItem, index) => (
                  <CheckItem key={checkItem.id} checkItem={checkItem} />
                ))}

              <CreateCheckItem
                checklist={checklist}
                onAddNewCheckItem={this.handleNewCheckItem}
              ></CreateCheckItem>
            </Card.Body>
          </Card>
        )}
      </>
    );
  }
}

export default Checklist;
