import React, { Component } from "react";
import { Card, CloseButton, Button, Form } from "react-bootstrap";
import Api from "../../api";

class CreateList extends Component {
  state = {
    listName: "",
    showForm: false,
  };

  handleSubmit = (event) => {
    event.preventDefault();

    const fetchData = async () => {
      try {
        const response = await Api.createAList(
          this.props.boardId,
          this.state.listName
        );
        this.props.onAddNewList(response.data);
        this.props.onGetMsg(response.data.name + " list added");
        this.setState({ listName: "" });
      } catch (error) {
        console.log(error);
      }
    };
    fetchData();
    this.handleClose();
  };

  handleOpen() {
    this.setState({ showForm: true });
  }

  handleClose() {
    this.setState({ showForm: false });
  }

  render() {
    return (
      <Card style={{ width: "350px" }}>
        {!this.state.showForm && (
          <Card.Body onClick={() => this.handleOpen()}>
            + Add another list
          </Card.Body>
        )}
        {this.state.showForm && (
          <Form onSubmit={this.handleSubmit}>
            <Form.Group controlId="listName">
              <Form.Control
                type="text"
                value={this.state.listName}
                onChange={(e) => this.setState({ listName: e.target.value })}
                placeholder="Enter list title"
              />
            </Form.Group>

            <Button
              disabled={this.state.listName === ""}
              variant="secondary"
              type="submit"
            >
              Add List
            </Button>
            <CloseButton onClick={() => this.handleClose()} />
          </Form>
        )}
      </Card>
    );
  }
}

export default CreateList;
