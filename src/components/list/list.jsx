import React, { Component } from "react";
import { Card, ListGroup, Dropdown } from "react-bootstrap";
import Api from "../../api";
import CreateCard from "../card/createCard";
import NotFound from "../utilities/notFound";
import Loader from "../utilities/loader";
import CardComp from "../card/card";

class List extends Component {
  state = {
    cards: [],
    loading: true,
    errorDisplay: false,
  };

  handleNewCard = (newCard) => {
    this.setState({ cards: [...this.state.cards, newCard] });
  };

  handleArchive() {
    const fetchData = async () => {
      try {
        const response = await Api.archiveAList(this.props.list.id, true);
        this.props.onGetMsg("list archived");
        this.props.onArchiveList(this.props.list.id);
      } catch (error) {
        console.log(error);
      }
    };
    fetchData();
  }

  handleDeleteCard = (cardId) => {
    this.setState({
      cards: this.state.cards.filter((card) => card.id != cardId),
    });
  };

  componentDidMount() {
    const fetchData = async () => {
      this.setState({ loading: true });
      try {
        const response = await Api.getAllCardsOfAList(this.props.list.id);
        this.setState({ cards: response.data });
      } catch (err) {
        this.setState({ errorDisplay: true });
      }
      this.setState({ loading: false });
    };
    fetchData();
  }

  render() {
    const { list } = this.props;
    return (
      <>
        {!this.state.errorDisplay && this.state.loading && <Loader></Loader>}
        {this.state.errorDisplay && <NotFound />}
        {!this.state.errorDisplay && (
          <Card style={{ width: "350px" }}>
            <Card.Header style={{ display: "flex" }}>
              <div style={{ flex: "85%" }}>{list.name}</div>
              <Dropdown style={{ flex: "15%" }}>
                <Dropdown.Toggle
                  style={{
                    backgroundColor: "transparent",
                    border: "none",
                    color: "black",
                  }}
                  id="dropdown-basic"
                ></Dropdown.Toggle>

                <Dropdown.Menu>
                  <Dropdown.Item>Add a card</Dropdown.Item>
                  <Dropdown.Item
                    onClick={() => {
                      this.handleArchive();
                    }}
                  >
                    Archive the list
                  </Dropdown.Item>
                </Dropdown.Menu>
              </Dropdown>
            </Card.Header>

            <Card.Body>
              <ListGroup>
                {this.state.cards &&
                  this.state.cards.length > 0 &&
                  this.state.cards.map((card) => (
                    <CardComp
                      key={card.id}
                      card={card}
                      onDeleteCard={this.handleDeleteCard}
                      onGetMsg={this.props.onGetMsg}
                    ></CardComp>
                  ))}
              </ListGroup>
              <Card.Footer>
                <CreateCard
                  listId={list.id}
                  onAddNewCard={this.handleNewCard}
                  onGetMsg={this.props.onGetMsg}
                />
              </Card.Footer>
            </Card.Body>
          </Card>
        )}
      </>
    );
  }
}

export default List;
